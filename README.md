

cd into the backend-extension-base folder

build:
	./gradlew jar                 (on linux)
	gradlew.bat jar               (on windows)

compile:
	java -jar compiler.jar PQLtest.java
	
run:
	java PQLtest
	
A empty set should have been created with the Query.

To run tests

copy compiler.jar to regression-tests and rename it to extendj.jar
cd into the regression-tests folder

run:
	ant -Dtest=PQL

License
-------
This repository is covered by the license BSD 2-clause, see file LICENSE.


Credits
-------
The following files in the repository come from:

- backend-extension-base/: files from Extendj Extension Base except files in bench, benchmarks, 
graph_scripts, pql_graph_scripts, results, results_YU, src and testfiles/PQL_test. 
The Extendj Extension Base files is licensed under BSD 2-clause.
- backend-extension-base/benchmarks/ and backend-extension-base/bench/: Performance benchmark for PQL written in Java.
- regression-tests/: files from Extendj regression tests excepts files in tests/PQL
The Extendj Extension regression tests files is licensed under BSD 2-clause.

