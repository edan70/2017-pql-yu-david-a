import java.util.*;

public class Test {
	public static void main(String[] args) {
    	  boolean b;
    	  Set<Integer> s = new HashSet<Integer>();
    	  s.add(5);
    	  s.add(-3);
    	  s.add(1);
          s.add(4);
    	  Set<Integer> t = new HashSet<Integer>();
    	  t.add(4);
    	  t.add(3);
    	  t.add(-1);
          t.add(-5);
    	  Set<Integer> result = new HashSet<Integer>();
          result = query(Set.contains(Integer x)):s.contains(x)&&exists y: t.contains(y) && y==x;
          System.out.println(result);
	}
}
