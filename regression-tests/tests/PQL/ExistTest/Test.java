import java.util.*;
public class Test {
	public static void main(String[] args) {
    	  boolean b;
    	  Set<Integer> s = new HashSet<Integer>();
    	  s.add(5);
    	  s.add(-3);
    	  s.add(1);
    	  Set<Integer> t = new HashSet<Integer>();
    	  t.add(-5);
    	  t.add(3);
    	  t.add(-1);
    	  b = exists x : s.contains(x) && t.contains(x) && x*x==1;
    	  System.out.println(b);
	}
}