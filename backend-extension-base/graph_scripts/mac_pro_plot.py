import matplotlib.pyplot as plt

a = range(1,7)
b = [2.6782512125, 1.3088032592, 2.8201774103, 1.2770247174, 0.7664322326, 0.81771848312]
conf_inter = [0.017526028, 0.005274048, 0.017058591, 0.007358073, 0.001751929, 0.003768616]
plt.errorbar(a, b, yerr=conf_inter, fmt='x')
#plt.plot(a, b, 'ro')
plt.axis([0, 7, 0, 3.0])
plt.title("Webgraph Benchmark test Intel Core i7 2.9GHZ (macbook pro)")
#plt.xlabel("evaluators")
plt.ylabel("Time in seconds")
plt.xticks([1, 2, 3, 4, 5, 6],["For loop", "PQL", "Stream", "Stream Parallel", "For loop Parallel", "PQL(Java6)"])
plt.show()
