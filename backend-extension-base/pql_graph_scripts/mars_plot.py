import matplotlib.pyplot as plt

a = range(1,6)
b = [2.2917946055, 1.5614473086, 2.3067526562, 1.5264425318, 0.6788770385]
conf_inter = [0.01766155, 0.015257642, 0.009266626, 0.023280568, 0.001499299]
colors = ['#624ea7', 'g', 'yellow', 'k', 'maroon']
plt.bar(a, b, yerr=conf_inter,color = colors)
#plt.plot(a, b, 'ro')
plt.axis([0, 6, 0, 3.0])
plt.title("Webgraph Benchmark test Intel Core i5-2500 3.30GHz x 4 (Mars)")
#plt.xlabel("evaluators")
plt.ylabel("Time in seconds")
plt.xticks([1, 2, 3, 4, 5],["For loop", "PQL", "Stream", "Stream Parallel", "For loop Parallel"])
plt.show()
