import matplotlib.pyplot as plt
import math
# webgraph
size = 50

a = range(1,7)
b = [2.7940829928*10**9, 1.31697100548*10**9, 8.8784709382*10**8, 8.4237613964*10**8, 2.98575791036*10**9, 7.8573770212*10**8] 
b = [i/10**9 for i in b]
six_colors = ['blue']*6
six_colors[1] = 'red'
conf_inter = [132701999, 53291763, 63653073, 58320958, 153151329, 11234686]
conf_inter = [i/math.sqrt(size) for i in conf_inter]
conf_inter = [i/10**9 for i in conf_inter]
# plt.errorbar(a, b, yerr=conf_inter, fmt='x')
plt.bar(a, b, yerr=conf_inter, color = six_colors)

#plt.plot(a, b, 'ro')
plt.axis([0, 7, 0, 3.5])
plt.title("Webgraph Benchmark test Intel Core i7 2.9GHZ (macbook pro)")
#plt.xlabel("evaluators")
plt.ylabel("Time in nanoseconds")
plt.xticks([1, 2, 3, 4, 5, 6],["For loop", "PQL",   "For loop Parallel", "Stream Parallel","Stream","PQL(Java6)"])
plt.show()
# threegrep
a = range(1,7)
b = [4478277.74, 2.95552738*10**7, 1978308.42, 1.048341552*10**7,  3.595576168*10**7, 2526076.06]
b = [i/10**7 for i in b]
conf_inter = [741573, 4668710, 208793, 749276, 1920395, 655477]
conf_inter = [i/math.sqrt(size) for i in conf_inter]
conf_inter = [i/10**7 for i in conf_inter]
# plt.errorbar(a, b, yerr=conf_inter, fmt='x')
plt.bar(a, b, yerr=conf_inter,color = six_colors)
#plt.plot(a, b, 'ro')
plt.axis([0, 7, 0, 4.0])
plt.title("Threegrep Benchmark test Intel Core i7 2.9GHZ (macbook pro)")
#plt.xlabel("evaluators")
plt.ylabel("Time in 10^7*nanoseconds")
plt.xticks([1, 2, 3, 4, 5, 6],["For loop", "PQL",   "For loop Parallel", "Stream Parallel","Stream","PQL(Java6)"])
plt.show()
# setnested
a = range(1,7)
b = [882636.6, 562107.3, 690017.92, 614231.32, 912687.66, 904987.56]
b = [i/10**6 for i in b]
conf_inter = [185130, 84865, 224227, 142210, 106230, 114932]
conf_inter = [i/math.sqrt(size) for i in conf_inter]
conf_inter = [i / 10**6 for i in conf_inter]
# plt.errorbar(a, b, yerr=conf_inter, fmt='x')
plt.bar(a, b, yerr=conf_inter, color = six_colors)
#plt.plot(a, b, 'ro')
plt.axis([0, 7, 0, 1.5])
plt.title("Setnested Benchmark test Intel Core i7 2.9GHZ (macbook pro)")
#plt.xlabel("evaluators")
plt.ylabel("Time in 10^6*nanoseconds")
plt.xticks([1, 2, 3, 4, 5, 6],["For loop", "PQL", "For loop parallel","Stream Parallel","Stream", "PQL(Java6)"])
plt.show()
# setexist
a = range(1,6)
b = [3692151.16, 1831667.94, 2502553.74, 1827008.06,4133140.88]
b = [i/10**6 for i in b]
conf_inter = [512649, 320176, 191543, 318385,201118]
conf_inter = [i/math.sqrt(size) for i in conf_inter]
conf_inter = [i / 10**6 for i in conf_inter]
# plt.errorbar(a, b, yerr=conf_inter, fmt='x')
plt.bar(a, b, yerr=conf_inter, color= six_colors[:5])
#plt.plot(a, b, 'ro')
plt.axis([0, 6, 0, 5.0])
plt.title("Setexist Benchmark test Intel Core i7 2.9GHZ (macbook pro)")
#plt.xlabel("evaluators")
plt.ylabel("Time in 10^6*nanoseconds")
plt.xticks([1, 2, 3, 4, 5],["For loop", "PQL", "For loop parallel","Stream Parallel","Stream"])


plt.show()