import matplotlib.pyplot as plt
import numpy as np
x = range(1,5)
para_manual = [2.96474116074, 1.46639447748, 1.00116044656, 0.76917881608]
pql = [1.96530982462, 1.58542597874, 1.50301361486, 1.35664649442]
stream = [1.61670385218, 1.274866563, 0.88048185842, 0.89254689582]
ms = 6
plt.plot(x, para_manual,label="para-manual",marker = 'x',markersize = ms)
plt.plot(x, pql,label="pql",marker = 'x',markersize = ms)
plt.plot(x,stream,label="stream",marker = 'x',markersize = ms)
plt.xticks(np.linspace(1,4,4))
plt.legend()
plt.title("performance-threads plot")
plt.xlabel("number of threads")
plt.ylabel("Time in second")
plt.show()