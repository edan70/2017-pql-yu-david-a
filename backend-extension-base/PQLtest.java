import java.util.stream.*;
import java.util.*;
import java.util.function.*;



public class PQLtest {
	public static void main(String[] args) {
		Set<Integer> y = new HashSet<Integer>();
		y.add(5);
		y.add(1);
		y.add(-3);
		//y = IntStream.range(0, 56).boxed().collect(Collectors.toSet());
		//Set<Integer> s = (new HashSet<Integer>()).stream().collect(Collectors.toSet());
		//Set<Integer> y = new HashSet<Integer>();
		Set<Integer> b = new HashSet<Integer>();
		Set<Person> c = new HashSet<>();
		c.add(new Person(5));
		c.add(new Person(1));
		c.add(new Person(-3));
		//System.out.println(System.getProperty("java.classpath"));
		//y.add(2);
		//y.add(4);
		//int x = 0;

		//b = PQLSetMethods.getIntegerSet(x -> y.contains(x));

		//b = PQLSetMethods.getIntegerSet((Predicate<Integer>) e -> y.contains(e));
/*		//  This can not practically be solved right? //
		if (rand() > 6)
			Binary a = new AddExpr();
		else
			Binary a = new SubExpr();
		query(Set.contains(x)):y.contains(a.method(x))*/

		//b = query(Set.contains(x)):(query(Set.contains(z)):y.contains(z) && z==5).contains(x) && x==-3;
		//boolean a = exists z : y.contains(z) && z==5;
		
		b = query(Set.contains(x)):y.contains(x) && c.contains(x);//exists z : y.contains(z) && z==5;
		//b = y.stream().parallel().filter(x -> c.contains(x)).collect(Collectors.toSet());
		//b = y.stream().parallel().filter((Predicate<Integer>) x -> y.stream().parallel().anyMatch((Predicate<Integer>) z -> z==2)).collect(Collectors.toSet());
		//boolean a = (query(Set.contains(x)):y.contains(3)).isEmpty();// y.contains(5);
		//Set<Integer> d = new HashSet<Integer>();
		//b = query(Set.contains(x)):y.contains(3) && y.contains(3);
		//b = query(Set.contains(objType x)):  x!=obj;
		//b = query(Set.contains(x)):;
		//y = query(Set.contains(x)):(b.contains(3) == true) && y.contains(x);
		//b = query(Set.contains(x)):y.contains(3);
		//b = query(Set.contains(x)):y.contains(3);	
		//b = query(Set.contains(x)):y.contains(3);
		//b = query(Set.contains(x)):y.contains(3);
		//b = query(Set.contains(x)):y.contains(3);
		//b = query(Set.contains(x)):y.contains(3);
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//y = y.stream().filter((Predicate<Integer>) e -> e < 3).collect(Collectors.toSet());
		//b = y.stream().filter((Predicate<Integer>) c -> c < 3).collect(Collectors.toSet());
		/*y = PQLSetMethods.getIntegerSet(new PQLExpression() {
			public boolean isTrue(int n) { return y.contains(n);}
			});*/
		//y = PQLSetMethods.getIntegerSet((Predicate<Integer>) e -> y.contains(e));
		for (Integer i : b)
			System.out.println("Integer: " + i);
		//y.forEach(System.out::println);
		//if ((query(Set.contains(x)):y.contains(3)).isEmpty())
		//int c = query(Set.contains(x)):x;
		//System.out.println("Integer: " + c);
		//boolean a = (query(Set.contains(x)):y.contains(3)).isEmpty();
		//a = (query(Set.contains(x)):y.contains(3)).isEmpty();
		//System.out.println("Set is empty: " + (query(Set.contains(x)):y.contains(3)).isEmpty());
	}

/*	public boolean predicate(int d) {
		return d < 3;
	}*/





}

class Person {
	public int a;
	public Person(int a) {
		this.a = a;
	} 
}

interface PQLExpression {
	public boolean isTrue(int n);
}



/*class PQLSetMethods {
	public static Set<Integer> getIntegerSet(PQLExpression predicate) {
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < 67; i++)
			if (predicate.isTrue(i))
				set.add(i);
		return set;
	}

}*/

class PQLSetMethods {
	public static Set<Integer> getIntegerSet(Predicate<Integer> predicate) {
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < 67; i++)
			if (predicate.test(i))
				set.add(i);
		return set;
	}

}
