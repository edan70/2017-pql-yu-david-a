
<YYINITIAL> {
   "query"        { yybegin(QUERY);return sym(Terminals.QUERY); }
   "exists"       { return sym(Terminals.EXISTS); }
}

<QUERY> {
  "Set"          { return sym(Terminals.SET); }
  "contains"     { return sym(Terminals.CONTAINS); }
  "("            { return sym(Terminals.LPAREN); }
  ")"            { return sym(Terminals.RPAREN); }
  "."            { return sym(Terminals.DOT); }
  "int"          { return sym(Terminals.INT); }
  "boolean"          { return sym(Terminals.BOOLEAN); }
  "double"          { return sym(Terminals.DOUBLE); }
  "float"          { return sym(Terminals.FLOAT); }
  ([:jletter:]|[\ud800-\udfff])([:jletterdigit:]|[\ud800-\udfff])* { return sym(Terminals.IDENTIFIER); }
  {WhiteSpace} 		{ }
    ":"             { yybegin(YYINITIAL);return sym(Terminals.COLON); }
//  "length"       { return sym(Terminals.LENGTH); }
//  "size"         { return sym(Terminals.SIZE); }
//  "get"          { return sym(Terminals.FIND); }
//  "=>"           { return sym(Terminals.IMPLY); }
  "["             { return sym(Terminals.LBRACK); }
  "]"             { return sym(Terminals.RBRACK); }
}

