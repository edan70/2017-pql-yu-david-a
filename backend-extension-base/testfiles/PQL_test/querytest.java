import java.util.stream.*;
import java.util.*;
import java.util.function.*;

public class QueryTest {
	public static void main(String[] args) {
		Set<Integer> y = new HashSet<Integer>();
		y.add(5);
		y.add(1);
		y.add(-3);
		y.add(4);
		Set<Integer> z = new HashSet<Integer>();
		z.add(3);
		z.add(1);
		z.add(-5);
		z.add(4);
		Set<Integer> a = new HashSet<Integer>();
		Set<Integer> b = new HashSet<Integer>();
		a = query(Set.contains(t)):y.contains(t);
		//a = query(Set.contains(t)):y.contains(t)&&z.contains(t)&&t<3;
		//b = y.stream().filter((Predicate<Integer>) c -> (z.contains(c)&&c<3)).collect(Collectors.toSet());
		System.out.println("a:"+a); //y
		System.out.println("b:"+b); //y&z
	} 
}