import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class TypeTest {
    public static void main(String[] args) {
        // Set<MyPair> s = new HashSet<>();
        // MyPair p = new MyPair(1,2);
        // MyPair q = new MyPair(3,4);
        // s.add(p);
        // Set<Webdoc> result = new HashSet<Webdoc>();
        //s.add(q);
        //System.out.println(s);
        // boolean b=true;
        // b = exists x: s.contains(x) && x==p;
        // b = exists MyPair x: s.contains(x);
        // b = s.stream().anyMatch((Predicate<Object>) x -> x==p);
        // Set<Webdoc> documents = new HashSet<Webdoc>();
        // Webdoc wa = new Webdoc(10);
        // Webdoc wb = new Webdoc(11);
        // Webdoc ww = new Webdoc(1);
        // Link lk = new Link(wa,wb);
        // ww.outlinks.add(lk);
        // documents.add(ww);
        // result = query(Set.contains(Webdoc doc)):
        //            documents.contains(doc) && exists link: doc.outlinks.contains(link);
        int size = 10;
        int count = 10;
        byte[][] data = new byte[count][];
        
        Random rand = new Random(42);

        for (int i = 0; i < count; i++) {
            byte[] d = new byte[size];
            for (int j = 0; j < size; j++)
                d[j] = (byte) (j);
            data[i] = d;
        }
        byte[][] array = data;
        // final int RECORD_SIZE = Generator.RECORD_SIZE;
        
        HashSet<byte[]> data_set = new HashSet<byte[]>();
        for (byte[] doc : data)
            data_set.add(doc);
        Set<Integer> number_set = new HashSet<Integer>();
        for (int i = 0; i<size-3 ; ++i) {
            number_set.add(i);
        }
        Set<byte[]> result;
        result = query(Set.contains(byte[] ba)): data_set.contains(ba) 
            && exists j:
               ba[j] == ((byte) 0)
            && ba[j + 1] == ((byte) 1)
            && ba[j + 2] == ((byte) 2)
            && number_set.contains(j);

        // result = query(Set.contains(byte[] ba)): exists i: array[i] == ba
        //     && exists j:
        //        ba[j] == ((byte) 0)
        //     && ba[j + 1] == ((byte) 1)
        //     && ba[j + 2] == ((byte) 2)
        //     && range(0, size - 3).contains(j);

        // result = data_set.stream().filter((Predicate<byte[]>) ba-> 
        //     number_set.stream().anyMatch((Predicate<Integer>) j -> 
        //                                ba[j] == ((byte) 0)
        //                             && ba[j + 1] == ((byte) 1)
        //                             && ba[j + 2] == ((byte) 2)))
        // .collect(Collectors.toSet());
    
        //System.out.println(b);
        System.out.println(data_set);
        for (byte[] bb : data_set ) {
            for (byte b : bb) {
                System.out.println(b==((byte) 0));
            }
            System.out.println();
        }
        System.out.println(result);

        System.out.println(number_set);
    }
    // private static Set<Integer> range(int begin, int end) {
    //     Set<Integer> s = new Set<Integer>();
    //     for (int i = begin; i<end ; ++i) {
    //         s.add(i);
    //     }
    //     return s;
    // }
}
// public class MyPair extends Object {
//     final int a;
//     final int b;
//     public MyPair(int a, int b) {
//         this.a = a;
//         this.b = b;
//     }
// }
// public class Webdoc
// {
//     public Set<Link> outlinks = new HashSet<Link>();
//     private static int idc = 0;
//     public int id = idc++;
//     public int[] words;

//     public Webdoc(int id) { }
// }
// public class Link
// {
//     private static int lid = 0;
//     public int linkid = lid++;
//     public Webdoc source;
//     public Webdoc destination;

//     public Link(Webdoc s, Webdoc t)
//     {
//         this.source = s;
//         this.destination = t;

//         source.outlinks.add(this);
//     }
// }