import java.util.stream.*;
import java.util.*;
import java.util.function.*;
public class ExistsTest {
	public static void main(String[] args) {
    	  boolean b;
    	  Set<Integer> s = new HashSet<Integer>();
    	  s.add(5);
    	  s.add(-3);
    	  s.add(1);
    	  Set<Integer> t = new HashSet<Integer>();
    	  t.add(-5);
    	  t.add(3);
    	  t.add(-1);
    	  b = exists x : s.contains(x) && t.contains(x) && x*x==1;
    	  System.out.println("b:"+b);
    	  boolean result;
    	  result = s.stream().anyMatch((Predicate<Integer>) x -> x==0);
    	  System.out.println("result:"+result);
	}
}