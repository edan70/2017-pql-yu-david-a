import java.util.stream.*;
import java.util.*;
import java.util.function.*;
public class ExistsQuery {
	public static void main(String[] args) {
    	  boolean b;
    	  Set<Integer> s = new HashSet<Integer>();
    	  s.add(5);
    	  s.add(-3);
    	  s.add(1);
          s.add(4);
    	  Set<Integer> t = new HashSet<Integer>();
    	  t.add(-5);
    	  t.add(3);
    	  t.add(-1);
          t.add(4);
    	  Set<Integer> result = new HashSet<Integer>();
          Set<Integer> sresult = new HashSet<Integer>();
          result = query(Set.contains(Integer x)):s.contains(x)&&exists y: t.contains(y) && y==x;
          //b = exists y: t.contains(y) && y==4 && s.contains(y);
          //sresult = s.stream().filter((Predicate<Integer>) x -> t.stream().anyMatch((Predicate<Integer>) y -> x==y)).collect(Collectors.toSet());
          System.out.println(result);
          System.out.println(sresult);
          //System.out.println(b);
	}
}
