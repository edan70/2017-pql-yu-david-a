package benchmarks.webgraph;
import benchmarks.Evaluator;
// own comment //import edu.umass.pql.container.*;
import java.util.*;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		// modified  PSet -> HashSet //
		Set<Webdoc> results = new HashSet<Webdoc>();
		//System.out.println("Doc de         st");
		for (Webdoc doc : Generator.documents) {
			if (results.contains(doc))
				continue;
		//	if (a++ % 1000 == 0)
			//	System.out.println("Doc outlinks: " + doc.outlinks.size());
			for (Link l : doc.outlinks) {
				for (Link l2 : l.destination.outlinks) {
					/*if (a++ % 10000000L == 0)
						System.out.println("Doc destination outlinks: " + l.destination.outlinks.size());*/
					if (l2.destination == doc) {
						results.add(doc);
						results.add(l.destination);
					}
				}
			}
		}
		result = results;
	}

	public String
	getName()
	{
		return "manual";
	}
}
