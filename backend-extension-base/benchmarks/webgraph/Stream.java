package benchmarks.webgraph;
import benchmarks.Evaluator;
// own comment //import static edu.umass.pql.Query;
// own comment //import static edu.umass.pql.Query.range;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;

public class Stream extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		Set<Webdoc> documents = Generator.documents;
		/*int j = 0;
		for (Webdoc d : Generator.documents) {
			if ((j + 1)%10000==0)
				System.out.println("Number first: " + j);
			j++;
		}*/

		result = documents.stream().parallel().filter(
							(Predicate<Webdoc>)x -> x.outlinks.stream().anyMatch(
								(Predicate<Link>)y -> y.destination.outlinks.stream().anyMatch(
									(Predicate<Link>)z -> z.destination == x)))
						.collect(Collectors.toSet());

	/*	int i = 0;
		for (Webdoc d : (Set<Webdoc>)result)
			System.out.println("Number: " + d.id);*/

	}

	public String getName() {
		return "Stream-" + para;
	}
}
