package benchmarks.webgraph;
import java.util.Set;

public class Link
{
	private static int lid = 0;
	public int linkid = lid++;
	public Webdoc source;
	public Webdoc destination;

	public Link(Webdoc s, Webdoc t)
	{
		this.source = s;
		this.destination = t;

		source.outlinks.add(this);
	}
}