package benchmarks.webgraph;
import benchmarks.Evaluator;
// own comment //import static edu.umass.pql.Query;
// own comment //import static edu.umass.pql.Query.range;
import java.util.*;


public class PQL extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		Set<Webdoc> documents = Generator.documents;

		result = query(Set.contains(Webdoc doc)):
		           documents.contains(doc)
			&& exists Link link: doc.outlinks.contains(link)
			&& exists Link link2 : link.destination.outlinks.contains(link2) && link2.destination == doc;


	}
}
