package benchmarks.threegrep;

import java.util.*;
import java.util.concurrent.*;

public class Generator extends benchmarks.GeneratorBase
{
	public static final int RECORD_SIZE = 100;
	public static byte[][] data_array;
	public static Set<byte[]> data_set;
	public static HashSet<Integer> index_set;

	public static void init()
	{
		data_array = generateData(RECORD_SIZE, getVar("THREEGREP_SIZE", SMALL_BENCHMARKS? 10 : (40000 * 1)));
		data_set = set(data_array);
		index_set = new HashSet<Integer>();
		for (int i = 0; i<=RECORD_SIZE-3; i++) {
			index_set.add(i);
		}
	}

	public static Set<byte[]>
	set(byte[][] data)
	{
		// HashSet<byte[]> retval = new HashSet<byte[]>();
		Set<byte[]> retval = Collections.newSetFromMap(new ConcurrentHashMap<byte[], Boolean>());
		for (byte[] doc : data)
			retval.add(doc);
		return retval;
	}

	public static byte[][]
	generateData(int size, int count)
	{
		byte[][] data = new byte[count][];
		Random rand = new Random(42);

		for (int i = 0; i < count; i++) {
			byte[] d = new byte[size];
			for (int j = 0; j < RECORD_SIZE; j++)
				d[j] = (byte) (32 + rand.nextInt(64));
			data[i] = d;
		}

		return data;
	}
}
