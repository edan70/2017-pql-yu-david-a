package benchmarks.threegrep;
import benchmarks.Evaluator;
// own comment //import static edu.umass.pql.Query;
// own comment //import static edu.umass.pql.Query.range;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;

public class SimpleStream extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		byte[][] array = Generator.data_array;
		final int RECORD_SIZE = Generator.RECORD_SIZE;

		// result = query(Set.contains(byte[] ba)): exists i: array[i] == ba
		// 	&& exists j:
		// 	   ba[j] == ((byte)'0')
		// 	&& ba[j + 1] == ((byte)'1')
		// 	&& ba[j + 2] == ((byte)'2')
		// 	&& range(0, RECORD_SIZE - 3).contains(j);

		Set<byte[]> data = Generator.data_set;
		Set<Integer> index = Generator.index_set;
		
		// System.out.println(array);
		// result = query(Set.contains(byte[] ba)): data.contains(ba) 
  //           && exists j:
  //              ba[j] == ((byte) '0')
  //           && ba[j + 1] == ((byte) '1')
  //           && ba[j + 2] == ((byte) '2')
  //           && index.contains(j);
		//System.out.println("stream is running");

		//stream normal//
		
        result = data.stream().filter((Predicate<byte[]>) ba ->
         	index.stream().anyMatch((Predicate<Integer>) j -> 
                ba[j] == ((byte) '0')
                && ba[j + 1] == ((byte) '1')
                && ba[j + 2] == ((byte) '2')))
        	.collect(Collectors.toSet());
        // System.out.println(results.size());
        //stream with one for loop//
        //System.out.println("stream is running");
  //       final HashSet<byte[]> results = new HashSet<byte[]>();
  //       for (byte[] ba : Generator.data_array) {
		// 	// if (index.stream().parallel().anyMatch((Predicate<Integer>) j -> 
  //  //              ba[j] == ((byte) '0')
  //  //              && ba[j + 1] == ((byte) '1')
  //  //              && ba[j + 2] == ((byte) '2'))) {
		// 	// 	results.add(ba);
		// 	for (int k = 0; k <= Generator.RECORD_SIZE - 3; k++)
		// 		if (ba[k] == '0' && ba[k + 1] == '1' && ba[k + 2] == '2') {
		// 			results.add(ba);
		// 			break;
		// 		}
				
		// 	}
		// }
		

	}
	public int
		getMaxParallelism()
		{
			return 1; //Env.DEFAULT_THREADS_NR;
		}

	public String getName() {

		return "Stream-Simple";
	}
}