package benchmarks.threegrep;
import benchmarks.Evaluator;
// own comment //import static edu.umass.pql.Query;
// own comment //import static edu.umass.pql.Query.range;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class PQL extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		byte[][] array = Generator.data_array;
		final int RECORD_SIZE = Generator.RECORD_SIZE;

		// result = query(Set.contains(byte[] ba)): exists i: array[i] == ba
		// 	&& exists j:
		// 	   ba[j] == ((byte)'0')
		// 	&& ba[j + 1] == ((byte)'1')
		// 	&& ba[j + 2] == ((byte)'2')
		// 	&& range(0, RECORD_SIZE - 3).contains(j);

		Set<byte[]> data = Generator.data_set;
		Set<Integer> index = Generator.index_set;
		
		 //System.out.println("PQL is running");
		result = query(Set.contains(byte[] ba)): data.contains(ba) 
            && exists j:
               ba[j] == ((byte) '0')
            && ba[j + 1] == ((byte) '1')
            && ba[j + 2] == ((byte) '2')
            && index.contains(j);

	}
}
