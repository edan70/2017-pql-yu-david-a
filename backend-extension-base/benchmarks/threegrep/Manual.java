package benchmarks.threegrep;
import benchmarks.Evaluator;
// own comment //import edu.umass.pql.container.*;
import java.util.*;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		// modified  PSet -> HashSet //
		final HashSet<byte[]> results = new HashSet<byte[]>();

		//System.out.println("Generator array size: " + Generator.data_array.length);

		for (byte[] ba : Generator.data_array) {
			for (int k = 0; k <= Generator.RECORD_SIZE - 3; k++)
				if (ba[k] == '0' && ba[k + 1] == '1' && ba[k + 2] == '2') {
					results.add(ba);
					break;
				}
		}

		this.result = results;
        // System.out.println(results.size());
	}

	public String
	getName()
	{
		return "manual";
	}
}
