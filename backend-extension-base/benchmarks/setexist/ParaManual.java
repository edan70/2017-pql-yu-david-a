package benchmarks.setexist;
import benchmarks.Evaluator;
import bench.RuntimeCreatorBench;

// own comment //import edu.umass.pql.container.PSet;
import java.util.*;
import java.util.concurrent.*;


public class ParaManual extends Evaluator.ParaEvaluator
{
	boolean results;
	Integer[] integer_array;

	class EvaluatorThread extends Thread
	{
		public EvaluatorThread(int start, int stop)
		{
			this.start = start;
			this.stop = stop;
		}
		int start, stop;

		public void run()
		{
			//Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
			//Set<Integer> generatedSet1 = generatedSet[0];
			Set<Integer> generatedSet2 = Generator.data_set.get(1);
Set<Integer> generatedSet3 = Generator.data_set.get(2);
Set<Integer> generatedSet4 = Generator.data_set.get(3);
Set<Integer> generatedSet5 = Generator.data_set.get(4);
Set<Integer> generatedSet6 = Generator.data_set.get(5);
Set<Integer> generatedSet7 = Generator.data_set.get(6);
Set<Integer> generatedSet8 = Generator.data_set.get(7);
Set<Integer> generatedSet9 = Generator.data_set.get(8);
Set<Integer> generatedSet10 = Generator.data_set.get(9);
			results = false;
			for (int i = start; i < stop; i++) {
				Integer x = integer_array[i];
				if ( 
					// x < 10 && 
generatedSet2.contains(x)&&
generatedSet3.contains(x)&&
generatedSet4.contains(x)&&
generatedSet5.contains(x)&&
generatedSet6.contains(x)&&
generatedSet7.contains(x)&&
generatedSet8.contains(x)&&
generatedSet9.contains(x)&&
generatedSet10.contains(x) 
&&x<10
				){
					results = true;
					break;
				}
			}

		}
	}

	@Override
	public Thread
	gen_thread(int notUsed, int start, int stop)
	{
		return new EvaluatorThread(start, stop);
	}

	@Override
	public int
	get_size()
	{
		return integer_array.length;
	}

	@Override
	public void
	compute_prepare()
	{
		// modified  PSet -> HashSet //
		//results = new PSet<Webdoc>(new Object[0x20000 * 2], 0); // make large enough to prevent resize, since that's currently a bit wobbly
		//results = new HashSet<Integer>(0x20000 * 2);
//Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>(0x20000 * 2));
			//Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
			Set<Integer> generatedSet1 = Generator.data_set.get(0);
			integer_array = generatedSet1.toArray(new Integer[generatedSet1.size()]);

		
		//results = new CopyOnWriteArraySet<Webdoc>();

	}

	@Override
	public Object
	compute_finish()
	{
		return results;
	}
}
