package benchmarks.setexist;
import benchmarks.Evaluator;
import bench.RuntimeCreatorBench;
//import edu.umass.pql.container.*;
//import static edu.umass.pql.Query;
// import static edu.umass.pql.Query.sumInt;
// import static edu.umass.pql.Query.range;
import java.util.*;

public class PQL extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		// Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
		//PMap<Integer, Integer>[] generatedMap = RuntimeCreatorBench.generatedMap;
		//List<int[]> generatedArray = RuntimeCreatorBench.generatedArray;
		// Set<Integer> generatedSet1 = generatedSet[0];
		// Set<Integer> generatedSet2 = generatedSet[1];
		// System.out.println(generatedSet1.getClass());
		// System.out.println(generatedSet.getClass());
		Set<Integer> generatedSet1 = Generator.data_set.get(0);
Set<Integer> generatedSet2 = Generator.data_set.get(1);
Set<Integer> generatedSet3 = Generator.data_set.get(2);
Set<Integer> generatedSet4 = Generator.data_set.get(3);
Set<Integer> generatedSet5 = Generator.data_set.get(4);
Set<Integer> generatedSet6 = Generator.data_set.get(5);
Set<Integer> generatedSet7 = Generator.data_set.get(6);
Set<Integer> generatedSet8 = Generator.data_set.get(7);
Set<Integer> generatedSet9 = Generator.data_set.get(8);
Set<Integer> generatedSet10 = Generator.data_set.get(9);


		 result = exists int x:  
		 // x < 10 && 
generatedSet1.contains(x) && 
generatedSet2.contains(x)&&
generatedSet3.contains(x)&&
generatedSet4.contains(x)&&
generatedSet5.contains(x)&&
generatedSet6.contains(x)&&
generatedSet7.contains(x)&&
generatedSet8.contains(x)&&
generatedSet9.contains(x)&&
generatedSet10.contains(x)
	&& x < 10
	 ;
	}
}
