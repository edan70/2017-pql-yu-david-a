package benchmarks.setexist;
import java.util.concurrent.*;
import java.util.*;

public class Generator extends benchmarks.GeneratorBase
{
	public static final int RECORD_SIZE = 100000;
	public static final int RECORD_NUM = 10;
	public static int[][] data_array;
	public static Set<Integer> data_set1;
	public static Set<Integer> data_set2;
	public static ArrayList<Set<Integer>> data_set;

	public static void init()
	{
		data_array = generateData(RECORD_SIZE, RECORD_NUM);

		data_set1 = set(data_array[0]);
		data_set2 = set(data_array[1]);
		// data_set3 = set(data_array[2]);
		// data_set4 = set(data_array[3]);
		// data_set5 = set(data_array[4]);
		// data_set6 = set(data_array[5]);
		// data_set7 = set(data_array[6]);
		// data_set8 = set(data_array[7]);
		// data_set9 = set(data_array[8]);
		// data_set10 = set(data_array[9]);

		data_set = set(data_array);
	}

	public static Set<Integer>
	set(int[] data)
	{
		// Set<Integer> retval = new HashSet<Integer>();
		Set<Integer> retval = Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>());
		for (int j = 0; j < RECORD_SIZE; j++) {
			retval.add(data[j]);
		}
		return retval;
	}

	public static ArrayList<Set<Integer>>
	set(int[][] data)
	{
		ArrayList<Set<Integer>> retval = new ArrayList<Set<Integer>>();
		for (int i = 0; i < RECORD_NUM; i++) {
			// Set<Integer> tmp = new HashSet<Integer>();
			Set<Integer> tmp = Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>());
			for (int j = 0; j < RECORD_SIZE; j++) {
				tmp.add(data[i][j]);
			}
			retval.add(tmp);
		}
		
		return retval;
	}

	public static int[][]
	generateData(int size, int count)
	{
		int[][] data = new int[count][];
		Random rand = new Random(42);

		for (int i = 0; i < count; i++) {
			int[] d = new int[size];
			for (int j = 0; j < RECORD_SIZE; j++)
				d[j] = rand.nextInt(RECORD_SIZE);
			data[i] = d;
		}

		return data;
	}
}
