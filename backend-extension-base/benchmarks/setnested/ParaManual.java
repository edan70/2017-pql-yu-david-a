package benchmarks.setnested;
import benchmarks.Evaluator;
import bench.RuntimeCreatorBench;

// own comment //import edu.umass.pql.container.PSet;
import java.util.*;
import java.util.concurrent.*;


public class ParaManual extends Evaluator.ParaEvaluator
{
	Set<Integer> results;
	Integer[] integer_array;

	class EvaluatorThread extends Thread
	{
		public EvaluatorThread(int start, int stop)
		{
			this.start = start;
			this.stop = stop;
		}
		int start, stop;

		public void run()
		{
			Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
			//Set<Integer> generatedSet1 = generatedSet[0];
			Set<Integer> generatedSet2 = generatedSet[1];

			for (int i = start; i < stop; i++) {
				Integer integer = integer_array[i];
				if (integer < 10 && generatedSet2.contains(integer))
				// if ( generatedSet2.contains(integer) && integer < 10)
					results.add(integer);
			}

		}
	}

	@Override
	public Thread
	gen_thread(int notUsed, int start, int stop)
	{
		return new EvaluatorThread(start, stop);
	}

	@Override
	public int
	get_size()
	{
		return integer_array.length;
	}

	@Override
	public void
	compute_prepare()
	{
		// modified  PSet -> HashSet //
		//results = new PSet<Webdoc>(new Object[0x20000 * 2], 0); // make large enough to prevent resize, since that's currently a bit wobbly
		results = new HashSet<Integer>(0x20000 * 2);
//Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>(0x20000 * 2));
			Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
			Set<Integer> generatedSet1 = generatedSet[0];
			integer_array = generatedSet1.toArray(new Integer[generatedSet1.size()]);

		
		//results = new CopyOnWriteArraySet<Webdoc>();

	}

	@Override
	public Object
	compute_finish()
	{
		return results;
	}
}
