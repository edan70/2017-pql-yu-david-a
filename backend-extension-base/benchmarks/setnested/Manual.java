package benchmarks.setnested;
import benchmarks.Evaluator;
import bench.RuntimeCreatorBench;
import benchmarks.setexist.Generator;
//import edu.umass.pql.container.*;
//import static edu.umass.pql.Query;
// import static edu.umass.pql.Query.sumInt;
// import static edu.umass.pql.Query.range;
import java.util.*;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		// Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
		// //PMap<Integer, Integer>[] generatedMap = RuntimeCreatorBench.generatedMap;
		// //List<int[]> generatedArray = RuntimeCreatorBench.generatedArray;
		// Set<Integer> generatedSet1 = generatedSet[0];
		// Set<Integer> generatedSet2 = generatedSet[1];
		Set<Integer> generatedSet1 = Generator.data_set1;
		Set<Integer> generatedSet2 = Generator.data_set2;

		Set<Integer> results = new HashSet<>();
		for (Integer i : generatedSet1)
			if (i < 10 && generatedSet2.contains(i))
			// if (generatedSet2.contains(i) && i < 10)
				results.add(i);		

		result = results;
	}

	public String
	getName()
	{
		return "manual";
	}
}
