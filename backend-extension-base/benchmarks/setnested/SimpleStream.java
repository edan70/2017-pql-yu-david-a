package benchmarks.setnested;
import benchmarks.Evaluator;
import benchmarks.setexist.Generator;
import bench.RuntimeCreatorBench;
//import edu.umass.pql.container.*;
//import static edu.umass.pql.Query;
// import static edu.umass.pql.Query.sumInt;
// import static edu.umass.pql.Query.range;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;

public class SimpleStream extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		//Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
		// Set<Integer> generatedSet1 = generatedSet[0];
		// Set<Integer> generatedSet2 = generatedSet[1];
		Set<Integer> generatedSet1 = Generator.data_set1;
		Set<Integer> generatedSet2 = Generator.data_set2;


		result = generatedSet1.stream().filter((Predicate<Integer>)x -> x < 10 && generatedSet2.contains(x)).collect(Collectors.toSet());
		// result = generatedSet1.stream().parallel().filter((Predicate<Integer>)x ->  generatedSet2.contains(x) && x < 10).collect(Collectors.toSet());

	}
public int
		getMaxParallelism()
		{
			return 1; //Env.DEFAULT_THREADS_NR;
		}
	public String getName() {
		return "Stream-Simple";
	}
}
