package benchmarks.setnested;
import benchmarks.Evaluator;
import benchmarks.setexist.Generator;
import bench.RuntimeCreatorBench;
//import edu.umass.pql.container.*;
//import static edu.umass.pql.Query;
// import static edu.umass.pql.Query.sumInt;
// import static edu.umass.pql.Query.range;
import java.util.*;

public class PQL extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		// Set<Integer>[] generatedSet = RuntimeCreatorBench.generatedSet;
		// //PMap<Integer, Integer>[] generatedMap = RuntimeCreatorBench.generatedMap;
		// //List<int[]> generatedArray = RuntimeCreatorBench.generatedArray;
		// Set<Integer> generatedSet1 = generatedSet[0];
		// Set<Integer> generatedSet2 = generatedSet[1];
		Set<Integer> generatedSet1 = Generator.data_set1;
		Set<Integer> generatedSet2 = Generator.data_set2;

		result = query(Set.contains(int x)): generatedSet1.contains(x) && x < 10 && generatedSet2.contains(x);
		// result = query(Set.contains(int x)): generatedSet1.contains(x) &&  generatedSet2.contains(x) && x < 10;
	}
}
